var express = require('express');
var router = express.Router();


// Set up connection pool with database
const { Pool } = require('pg');
const pool = new Pool({
    user: 'postgres',
    database: 'xamarinwebchat'
});

// pool will emit error on idle clients
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Returns all of the users in the database
router.get('/users/all', function(req, res, next){

    console.log(req.query);

    var currentUserId = req.query.id

    console.log("Getting users for user number " + currentUserId);

    pool.query("SELECT * FROM users WHERE id <> $1", [currentUserId])
        .then(function(data){
            console.log(data.rows);
            res.json(data.rows);
        })
        .catch(function(err){
            console.log(err);
        })
})

// Find a conversation between two users
router.get('/conversation/single', function(req, res, next){

    var currentUserId = req.query.id1;
    var selectedUserId = req.query.id2;

    var newConversationId;

    console.log(req.query);
    
    pool.query(`SELECT DISTINCT loggedinconversations.id
                FROM (
                    SELECT DISTINCT c.id
                    FROM participants AS p, conversations AS c 
                    WHERE p.user_id = $1 AND c.is_group = false
                ) AS loggedinconversations, participants AS p
                WHERE p.user_id = $2 AND p.conversation_id = loggedinconversations.id
                `, [currentUserId, selectedUserId])
        .then(function(data){
            if(data.rows.length > 0)
            {
                console.log("Conversation found!");
                console.log(data.rows);
                res.json([{conversationId: data.rows[0].id}])
            } else
            {
                console.log("No conversation found, making a new one.")
                

                var newConversationId;
                var userIdArray = [currentUserId, selectedUserId];

                // First, create a new conversation 
                pool.query('INSERT INTO conversations VALUES (DEFAULT) RETURNING id')
                    .then(function(data){
                        newConversationId = data.rows[0].id; 
                        console.log("New conversation created with id of " + newConversationId);

                        // Add participants 
                        userIdArray.forEach(function(userId){
                            pool.query('INSERT INTO participants VALUES (DEFAULT, $1, $2)', [userId, newConversationId])
                            .catch(function(err){
                                console.log(err);
                            })
                        })

                        res.json([{ conversationId: newConversationId }]);
                    })
                    .catch(function(err){
                        console.log(err);
                    })    
            }
        })
})

// When given a conversation id, finds all messages with that id
router.get('/conversation/id', function(req, res, next){
    
    var conversationId = req.query.id;

    
    pool.query('SELECT * FROM messages WHERE conversation_id= $1', [conversationId])
        .then(function(data){
            res.json(data.rows);
        })
        .catch(function(err){
            console.log(err);
        })
})


router.post('/newmessage', function(req, res, next) {
    console.log("New message received");
    console.log(req.body);
 
    pool.query('INSERT INTO messages VALUES (DEFAULT, $1, $2, $3, DEFAULT) RETURNING id', [req.body.sender_id, req.body.conversation_id, req.body.body])
        .then(function(){
            console.log("Successfully added new message"); 
        })
        .catch(function(err){
            console.log(err);
        })

    res.json("New Message Post");     
})

// This route will fetch all of the conversations of a user when given an id
router.get('/getconversationids/', function(req, res, next){

    var currentUserId = req.query.id;
    var conversationsToReturn = [];
    var itemsProcessed = 0;

    console.log("Getting conversatiosn for user #" + currentUserId);
    pool.query('SELECT conversation_id FROM participants WHERE user_id = $1', [currentUserId])
        .then(function(data){
            console.log("Logged in user's conversations:");
            console.log(data.rows);
          
            data.rows.forEach(function(id, index, array){
                pool.query('SELECT * FROM conversations WHERE id = $1', [id.conversation_id])
                    .then(function(data){
                        conversationsToReturn.push(data.rows[0]);

                        itemsProcessed = itemsProcessed + 1;
                        if(itemsProcessed == array.length){
                            callback(res, conversationsToReturn); 
                        }

                    }) 
                    .catch(function(err){
                        console.log(err);
                    })
            })
        })
        .catch(function(err){
            console.log(err);
        })

})


router.post('/groupconversation', function(req, res, next){
    
    console.log("Creating a new group conversation");
    console.log(req.body);

    var userIdArray = req.body.ids;
    var newConversationTitle = req.body.title;
    var newConversationId = -1;

    // First, create a new conversation 
    pool.query('INSERT INTO conversations VALUES (DEFAULT, $1, TRUE) RETURNING id', [newConversationTitle])
        .then(function(data){
            newConversationId = data.rows[0].id; 
            console.log("New conversation created with id of " + newConversationId);

            // Add participants 
            userIdArray.forEach(function(userId){
                pool.query('INSERT INTO participants VALUES (DEFAULT, $1, $2)', [userId, newConversationId])
                .catch(function(err){
                    console.log(err);
                })
            })

            res.json([{ConversationID: newConversationId}]);
        })
        .catch(function(err){
            console.log(err);
        })    
 
});

// Helper functions
// Will take an array of user IDs and generate the participants and conversationID 
function makeNewConversation(userIdArray){
    var newConversationId;

    console.log("In helper.");

    // First, create a new conversation 
    pool.query('INSERT INTO conversations VALUES (DEFAULT) RETURNING id')
        .then(function(data){
            newConversationId = data.rows[0].id; 
            console.log("New conversation created with id of " + newConversationId);

            // Add participants 
            userIdArray.forEach(function(userId){
                pool.query('INSERT INTO participants VALUES (DEFAULT, $1, $2)', [userId, newConversationId])
                .catch(function(err){
                    console.log(err);
                })
            })
        })
        .catch(function(err){
            console.log(err);
        })    


    return newConversationId;
}

function callback(res, output){
    console.log(output);
    res.json(output); 
}

module.exports = router;
