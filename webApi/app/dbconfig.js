const { Pool, Client } = require('pg')
const promise = require('bluebird');

const initOptions = {
    promiseLib: promise
};
const pgp = require('pg-promise')(initOptions);

const cn = {
    user: 'postgres',
    database: 'xamarinwebchat',
    password: 'postgres',
    port: 5432
}

const db = pgp(cn)

module.exports = db;
