
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {id: 1, username: 'bauer@creep.im', firstname: 'Bauer', lastname: 'Creep 1'},
        {id: 2, username: 'bauer2@creep.im', firstname: 'Bauer', lastname: 'Creep 2'},
        {id: 3, username: 'bauer1@localhost', firstname: 'Bauer', lastname: 'localhost 1'},
        {id: 4, username: 'bauer2@localhost', firstname: 'Bauer', lastname: 'localhost 2'}
      ]);
    });
};
