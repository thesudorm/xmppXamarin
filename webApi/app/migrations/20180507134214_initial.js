exports.up = function(knex, Promise) {

    return Promise.all([
        
        knex.schema.createTable('users', function(table){
            table.increments('id').primary();
            table.string('username', 20);
            table.string('firstname', 20);
            table.string('lastname', 20);
        }),

        knex.schema.createTable('messages', function(table){ 
            table.increments('id').primary();
            table.integer('sender_id');
            table.integer('conversation_id');
            table.string('body', 180);
            table.timestamp('created_at').defaultTo(knex.fn.now());
        }),

        knex.schema.createTable('participants', function(table){ 
            table.increments('id').primary();
            table.integer('user_id');
            table.integer('conversation_id');
            
        }),
        
        knex.schema.createTable('conversations', function(table){  
            table.increments('id').primary();
            table.string('display_name', 80).defaultTo('Replace Me');
            table.boolean('is_group').defaultTo(false);
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('users'),
        knex.schema.dropTable('messages'),
        knex.schema.dropTable('participants'),
        knex.schema.dropTable('conversations')
    ]) 
};
