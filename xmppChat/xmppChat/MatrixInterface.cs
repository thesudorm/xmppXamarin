﻿using System;
using System.ComponentModel;
using Matrix.Xmpp.Client;
using System.Security.Authentication;
using xmppChat.MatrixExtentions;

namespace xmppChat
{
	public sealed class MatrixInterface
    {
		private static readonly MatrixInterface instance = new MatrixInterface();
		public XmppClient client;
       
		private MatrixInterface()
		{
			client = new XmppClient();

            // Registers the custom elements that I have created
            RegisterCustomElements();

            client.OnError += Client_OnError;
            client.OnSendXml += Client_OnSendXml;
            client.OnReceiveXml += Client_OnReceiveXml;


			Matrix.License.LicenseManager.SetLicense(MatrixConfig.lic);
            client.TlsProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            // testing to see if it is a tls error I am having
            //client.TlsProtocols = SslProtocols.None;

            // Give the signed in user's credentials to the xmpp client
            client.SetUsername(ChatConfig.username);
            client.SetXmppDomain(ChatConfig.xmppDomain);
            client.Password = ChatConfig.password; 
            client.Hostname = ChatConfig.hostname;

            client.Open();


		}

        private void Client_OnReceiveXml(object sender, Matrix.TextEventArgs e)
        {
            Console.WriteLine("INCOMING: " + e.Text);
        }

        private void Client_OnSendXml(object sender, Matrix.TextEventArgs e)
        {
            Console.WriteLine("OUTGOING: " + e.Text);
        }

        private void Client_OnError(object sender, Matrix.ExceptionEventArgs e)
        {
            var err = e;
            var result = e.ToString();
        }

        public static MatrixInterface Instance
		{
			get
			{
				return instance;
			}
		}

        private static void RegisterCustomElements()
        {
            Matrix.Xml.Factory.RegisterElement<Result>("urn:xmpp:mam:2", "result");
            Matrix.Xml.Factory.RegisterElement<Field>("field");
            Matrix.Xml.Factory.RegisterElement<MamX>("jabber:x:data", "x");
        }
    }
}
