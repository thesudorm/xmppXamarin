﻿using System;
using static xmppChat.MatrixDependencyService;
using System.IO;
using Matrix.Xmpp.Client;
using Matrix.Xmpp;
using System.Security.Authentication;
using xmppChat;

[assembly: Xamarin.Forms.Dependency(typeof(MatrixImplementation))]
namespace xmppChat
{
    public class MatrixImplementation : IMatrixDependencyService
    {

        public MatrixImplementation() { }

        public XmppClient xmppClient = new XmppClient();

        // Authentication for the client
        public string debugText;
        public string path = "/Users/numberthree/xamarinWebchat/logs.txt";

        string lic = @"eJxkkFlT4kAQx7+K5fvuJLEQsNopy2QQLELAJCvyNpIhDMwR5uDw0292Rd3j
pau7f338u2HMl0xZdnGUQtnbS1p/s3rlDtSwG/GOLjFMja780o0qnDtfcQ3o
KwMzT5Xj7oRDQJ8+xN46LZnBMKGSYbKnwlOnDaDfMcRaNlSdPgDX6uIsBdAH
AyIpF9hSwezdH8q+V23RO2uLPxeVTUUdI8eGG5a0Ho6CsB90gwjQfwhGNmFS
Y2d8O+scwC/7d38v6AXtXf8AyHmtqPOG4VQvog1xqjyuxi555skysYPdoh7u
skxcz9ciKev77JAKkcl1sV2QfMv6fpP38rTQwyxcJbvuImXm8WGa5DSamk7Z
jAxK43XEJ4xlR67qYn5vTXo9G0jp5nT09HSaxYYPSLDZlw/x0L4Nn016Nem+
jq8C9Nr/UZBDGBPVb+rqMN93wq0nL0Xn5RbQl25A53fjnwI=";


        void IMatrixDependencyService.Connect()
        {

            try
            {

                //Matrix.License.LicenseManager.SetLicense(lic);
				xmppClient.TlsProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

                xmppClient.Username = "bauer";
                xmppClient.XmppDomain = "creep.im";
                xmppClient.Password = "password";

                xmppClient.Open();


            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }

        void IMatrixDependencyService.Disconnect()
        {
			xmppClient.Close();
        }


        // Helper function for writing logs to a file
        void AddDebug(string debug)
        {
            if (!File.Exists(path))
            {
                File.Create(path);
                TextWriter textWriter = new StreamWriter(path);
                textWriter.WriteLine(debug);
                textWriter.Close();
            }
            else if (File.Exists(path))
            {
                using (var textWriter = new StreamWriter(path, true))
                {
                    textWriter.WriteLine(debug);
                }
            }
        }

    }
}