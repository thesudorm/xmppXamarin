﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Windows.Input;
using xmppChat.Models;
using Xamarin.Forms;
using Matrix.Xmpp;
using xmppChat;
using Matrix.Xmpp.Client;
using Matrix.License;
using System.Security.Authentication;
using Newtonsoft.Json;
using Message = Matrix.Xmpp.Client.Message;
using xmppChat.MatrixExtentions;
using Matrix.Xmpp.Delay;
using Matrix.Xmpp.MessageCarbons;
using System.IO;
using Matrix.Xmpp.Muc;

namespace xmppChat.ViewModels
{

    public class ConversationViewModel : BaseViewModel
    {
        #region Event Handlers

        // On Message is triped in two different scenarios
        // 1) To get chat history from server
        // 2) To get messages in real time sent by the other user
        void Client_OnMessage(object sender, MessageEventArgs e)
		{

            // check to see if the incoming message has a result tag or not
            // If it does have a result tag, then the message is a part of MAM
            // It will need to be parsed differently
            //var message = new Matrix.Xmpp.Client.Message();
            var message = e.Message;
            DateTime messageStamp = DateTime.Now;

            if (e.Message.HasTag<Result>()) // Message is from MAM
            {
                var result = e.Message.Element<Result>();
                message = result.Element<Matrix.Xmpp.MessageCarbons.Forwarded>().Element<Message>();
                var delay = result.Element<Forwarded>().Element<Delay>();

                if (delay != null)
                {
                    messageStamp = delay.Stamp;
                }

            }
            else
            { // Message that is received in real time
                message = e.Message;
            }

            // Parse the jid out of the From property of the message
            // The server also sends the resource with the jid, but 
            // having the resource screws up the comparison, so we cut it
            string[] toParse = message.From.ToString().Split('/');

            // Build the message object that will be added to the observable collection
            var messageToAppend = new Models.Message
            {
                Id = message.Id,
                Sender_Jid = toParse[0],
                Body = message.Body,
                Created_At =  messageStamp
            };
            conversation.Insert(conversation.Count, messageToAppend);
		}

		void Client_OnError(object sender, Matrix.ExceptionEventArgs e)
		{
			Console.Write(e.Exception);
		}

        #endregion

        #region Variables

        public string displayName;
		public string Jid;

		public ObservableCollection<Models.Message> conversation = new ObservableCollection<Models.Message>();
		public XmppClient xmppClient;

        public Matrix.Xmpp.MessageArchiving.Retrieve testList = new Matrix.Xmpp.MessageArchiving.Retrieve();

        #endregion

        #region Constructors

        public ConversationViewModel(string toDisplay, string jid)
        {
            displayName = toDisplay;
			Jid = jid;

            GetChatHistoryAsync();

            // Add Messegaes listener
			MatrixInterface.Instance.client.OnMessage += Client_OnMessage;
            MatrixInterface.Instance.client.OnError += Client_OnError;

        }


        #endregion

        #region Properties

        public string DisplayName
        {
            get { return this.displayName; }
            set { SetValue(ref displayName, value); }

        }

		public ObservableCollection<xmppChat.Models.Message> Conversation
        {
            get { return this.conversation; }
            set { SetValue(ref conversation, value); }
        }


		#endregion

		#region Commands

		//private Command _sendMessageCommand
		//public ICommand SendMessageCommand
		//{
		//	get {
		//		if (_sendMessageCommand == null)
		//		{
		//			_sendMessageCommand = new Command(NewMessage);
		//		}

		//		return _sendMessageCommand;
		//	}
		//}

        #endregion

		#region Functions

		public void GetMessagesAsync()
		{
            // TODO: Need to use mod_mam to get these messages, was using my api for this

            return;
		}

        // Called when the user sends a message
        public void SendMessage(string inputBody)
        {
			var msg = new Matrix.Xmpp.Client.Message
			{
				Type = MessageType.Chat,
				To = Jid,
				Body = inputBody
			};

			try {
				MatrixInterface.Instance.client.Send(msg);

                //This appends the new message to the end of the converstion list
                var messageToAppend = new Models.Message()
                {
                    Sender_Jid = ChatConfig.jid,
                    Body = inputBody,
                    Created_At = DateTime.Now
                };

                conversation.Insert(conversation.Count, messageToAppend);

			} catch(Exception err){
				Console.Write(err);	
			}
        }

        public async System.Threading.Tasks.Task GetChatHistoryAsync()
        {
            XmppClient xmppClient = MatrixInterface.Instance.client;

            // Create a query to find all of the messages for the logged in user
            var query = new Matrix.Xml.XmppXElement("urn:xmpp:mam:2", "query");

            //var test = new Matrix.Xml.XmppXElement("urn:xmpp:mam:2", "query");
            //test.AddTag("jabber:x:data", "x", "");
            //test.Element<MamX>().AddTag("field");
            //test.Element<Field>().Var = "with";
            //test.Element<Field>().Value = Jid;

            
            var test = new Matrix.Xml.XmppXElement("urn:xmpp:mam:2", "query");
            var mamx = new Matrix.Xml.XmppXElement("urn:xmpp:mam:2", "query");
            


            var iq = new Matrix.Xmpp.Client.Iq
            {
                Type = Matrix.Xmpp.IqType.Set,
                Query = query // This needs to equal to my new query element
            };

            iq.GenerateId();
            //xmppClient.IqFilter.SendIq(iq, GetChatHistoryCallback);
            Iq result = await xmppClient.IqFilter.SendIqAsync(iq);

        }

        private void GetChatHistoryCallback(object sender, IqEventArgs args)
        {
            // Do nothing
        }

        #endregion
    }
}