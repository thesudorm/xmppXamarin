﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using xmppChat.Models;
using System.Net.Http;
using System.Text;
using Xamarin.Forms;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace xmppChat.ViewModels
{
    class ConversationsViewModel : BaseViewModel
    {
		#region Variables


		public ObservableCollection<Conversation> conversations = new ObservableCollection<Conversation>();
		private Conversation selectedConversation;
		public INavigation Navigation;

		#endregion

		#region Constructor

		public ConversationsViewModel()
		{
		}

		#endregion

		#region Properties

		public ObservableCollection<Conversation> Conversations
		{
			get { return this.conversations; }
			set { SetValue(ref conversations, value); }
		}

		public Conversation SelectedConversation
		{
			get { return selectedConversation; }
			set { SetValue(ref selectedConversation, value); }
		}

		#endregion

		#region Commands


		#endregion

		#region Functions


		#endregion
	}
}
