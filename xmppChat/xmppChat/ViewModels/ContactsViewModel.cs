﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Windows.Input;
using Newtonsoft.Json;
using Xamarin.Forms;
using xmppChat.Models;
using xmppChat.Utilities;

namespace xmppChat.ViewModels
{
	public class ContactsViewModel : BaseViewModel
	{
		void Client_OnRosterItem(object sender, Matrix.Xmpp.Roster.RosterEventArgs e)
		{
			Console.Write(sender);
			Console.Write(e.RosterItem.Jid);
			Console.Write(e);

			this.contacts.Add(new Contact()
			{
				Firstname = e.RosterItem.Name,
				Lastname = "",
				Username = e.RosterItem.Jid,
                Jid = e.RosterItem.Jid
            });


			SelectableContacts = new ObservableCollection<SelectableItemWrapper<Contact>>(contacts.Select(cn => new SelectableItemWrapper<Contact> { Item = cn }));
		}


		#region Variables

		public ObservableCollection<Contact> contacts = new ObservableCollection<Contact>();
		public ObservableCollection<SelectableItemWrapper<Contact>> selectableContacts;

		public INavigation Navigation;


		#endregion

		#region Contructor

		public ContactsViewModel()
		{
			// GetContacts();
			// Getting Roster from client

			MatrixInterface.Instance.client.OnRosterItem += Client_OnRosterItem;

			//GetContactsAsync();
		}


        #endregion

        #region Properties

        public ObservableCollection<SelectableItemWrapper<Contact>> SelectableContacts
		{
			get { return this.selectableContacts; }
			set { SetValue(ref selectableContacts, value); }
		}

		public ObservableCollection<Contact> Contacts
		{
			get { return this.contacts; }
			set { SetValue(ref contacts, value); }
		}

		#endregion

		#region Functions

		#endregion

		#region Commands

		#endregion
	}
}
