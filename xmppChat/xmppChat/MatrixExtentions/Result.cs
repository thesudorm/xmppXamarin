﻿using System;
using Matrix.Xml;

namespace xmppChat.MatrixExtentions
{
    public class Result : XmppXElement
    {
        public Result() : base("urn:xmpp:mam:2", "result")
        {
        }

        public int Id
        {
            get { return GetAttributeInt("id"); }
        }

        public Matrix.Xmpp.MessageCarbons.Forwarded forwarded
        {
            get { return Element<Matrix.Xmpp.MessageCarbons.Forwarded>(); }
        }


    }
}