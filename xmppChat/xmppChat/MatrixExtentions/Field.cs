﻿using Matrix.Xml;

namespace xmppChat.MatrixExtentions
{
    class Field : XmppXElement
    {
        public Field() : base("field")
        {
        }

        public string Var
        {
            set { SetAttribute("var", value); }
            get { return GetAttribute("var"); }
        }

        public new string Value
        {
            set { SetTag("value", value); }
            get { return GetTag("value"); }
        }

    }
}
