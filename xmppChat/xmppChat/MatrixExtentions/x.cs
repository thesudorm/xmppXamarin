﻿using Matrix.Xml;

namespace xmppChat.MatrixExtentions
{
    class MamX : XmppXElement
    {
        public MamX() : base("jabber:x:data", "x")
        {

        }
        
        public string Type
        {
            set { SetAttribute("type", value); }
            get { return GetAttribute("type"); }
        }
    }
}
