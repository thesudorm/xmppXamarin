using System;
using System.Security.Authentication;
using Matrix.License;
using Matrix.Xmpp.Client;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static xmppChat.MatrixDependencyService;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace xmppChat
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

			MainPage = new NavigationPage(new MainPage());

            // Create the Singleton Client Instance
			var instance = MatrixInterface.Instance;

        }

        protected override void OnStart()
        {
			// Handle when your app starts
			// Declare a global xmpp client

			var xmppClient = new XmppClient();


        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
