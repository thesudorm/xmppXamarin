﻿using System;

namespace xmppChat
{
    public class MatrixDependencyService
    {
        public interface IMatrixDependencyService
        {
            void Connect();
            void Disconnect();
        }
    }
}