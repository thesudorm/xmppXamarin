﻿using System;
using Xamarin.Forms;

using xmppChat.ViewModels;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;
using xmppChat.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace xmppChat.Utilities
{
    public class SelectableContactCell : ViewCell
    {
        #region Variables

        public static readonly BindableProperty FirstNameProperty =
            BindableProperty.Create("FirstName", typeof(string), typeof(SelectableContactCell), "ReplaceMe");

        public static readonly BindableProperty LastNameProperty =
            BindableProperty.Create("LastName", typeof(string), typeof(SelectableContactCell), "ReplaceMe");

        public static readonly BindableProperty IdProperty =
            BindableProperty.Create("Id", typeof(int), typeof(SelectableContactCell), -1);

		public static readonly BindableProperty JidProperty =
            BindableProperty.Create("Jid", typeof(string), typeof(SelectableContactCell), "ReplaceMe");
		
        public string FirstName
        {
            get { return (string)GetValue(FirstNameProperty); }
            set { SetValue(FirstNameProperty, value); }
        }

        public string LastName
        {
            get { return (string)GetValue(LastNameProperty); }
            set { SetValue(LastNameProperty, value); }
        }

        public int Id
        {
            get { return (int)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public string Jid
		{
			get { return (string)GetValue(JidProperty); }
			set { SetValue(JidProperty, value); }
		}
      

        public HttpClient htppClient = new HttpClient();

        Label lbFirst, lbLast, lbId, lbUsername, lbJid;

        Button chatButton;

        #endregion

        #region Constructors

        public SelectableContactCell()
        {
            lbFirst = new Label { HorizontalOptions = LayoutOptions.StartAndExpand };
            lbLast = new Label { HorizontalOptions = LayoutOptions.CenterAndExpand };
            lbId = new Label { HorizontalOptions = LayoutOptions.EndAndExpand };
			lbUsername = new Label { HorizontalOptions = LayoutOptions.EndAndExpand };
			lbJid = new Label { HorizontalOptions = LayoutOptions.EndAndExpand };

            chatButton = new Button();
            chatButton.Text = "Chat";
            chatButton.Command = GetConversationCommand;

            Grid layout = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)},
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)},
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star)}
                },
                HorizontalOptions = LayoutOptions.FillAndExpand
            };

            layout.Children.Add(lbFirst, 0, 0);
            layout.Children.Add(lbLast, 1, 0);
            layout.Children.Add(chatButton, 2, 0);

            var cellWrapper = new Grid
            {
                Padding = 10,
                ColumnDefinitions =
                {
                    new ColumnDefinition {Width = new GridLength(1,GridUnitType.Auto)},
                    new ColumnDefinition {Width = new GridLength(1,GridUnitType.Star)},
                }
            };

            var sw = new Switch();
            sw.SetBinding(Switch.IsToggledProperty, "IsSelected");

            cellWrapper.Children.Add(sw, 0, 0);
            cellWrapper.Children.Add(layout, 1, 0);

            View = cellWrapper;
        }

        #endregion

        #region Commands


        public ICommand getConversationCommand;
        public ICommand GetConversationCommand
        {
            get
            {
                return getConversationCommand ??
                    (getConversationCommand = new Command(
                        async () =>
                        {
							//var conversationId = await GetConversationId();
                            // I don't need to get the conversation id anymore, the idea is to have the
                            // conversation view model do this with the mod_mam functionality
					        await App.Current.MainPage.Navigation.PushAsync(new ConversationPage(lbFirst.Text, lbJid.Text));
                        }));
            }
        }

        #endregion

        #region Functions
        
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext != null)
            {
                lbFirst.Text = FirstName;
                lbLast.Text = LastName;
                lbId.Text = Id.ToString();
                lbJid.Text = Jid;
            }
        }

        #endregion
    }
}
