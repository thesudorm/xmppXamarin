﻿using Xamarin.Forms;
using xmppChat.Models;

namespace xmppChat.Utilities
{
	public class MessageDataTemplateSelector : DataTemplateSelector
    {
		public DataTemplate OutgoingTemplate { get; set; }
		public DataTemplate IncomingTemplate { get; set; }

		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			return ((Message)item).Sender_Jid == ChatConfig.jid ? OutgoingTemplate : IncomingTemplate;
		}

    }
}
