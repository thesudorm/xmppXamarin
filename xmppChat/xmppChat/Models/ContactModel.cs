﻿using System;
namespace xmppChat.Models
{
    public class Contact
    {
		public int Id { get; set; }
        public String Username { get; set; }
        public String Firstname { get; set; }
        public String Lastname { get; set; } 
        public String Jid { get; set; } 
    }
}
