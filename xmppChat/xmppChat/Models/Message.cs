﻿using System;
namespace xmppChat.Models
{
	public class Message
	{

		#region Variables

		public String Id { get; set; }
		public string Sender_Jid { get; set; }
		public String Body { get; set; }
		public DateTime Created_At { get; set; }

		#endregion

		#region Contructor

        // Default constructor
		public Message()
		{
            this.Id = "";
			this.Sender_Jid = "";
			this.Body = "";
            this.Created_At = DateTime.Now;
		}

        #endregion
	}
}
