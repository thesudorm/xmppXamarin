﻿using System;
using Matrix.Xmpp.Client;
using System.Security.Authentication;
using Matrix.Xmpp;
using Matrix.License;

namespace xmppChat
{
    public class MatrixService
    {
		void XmppClient_OnError(object sender, Matrix.ExceptionEventArgs e)
		{
			Console.Write("ERR");
		}


		public XmppClient xmppClient;

		public MatrixService()
        {
			// Initialize the client
			xmppClient = new XmppClient();

			xmppClient.OnError += XmppClient_OnError;
            // Set the license and adjust the security protocol
		   // LicenseManager.SetLicense(MatrixConfig.lic);
			xmppClient.TlsProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            // Hard code the user data
			xmppClient.Username = "bauer";
            xmppClient.XmppDomain = "creep.im";
            xmppClient.Password = "password";

            // Open the client
			try {
				xmppClient.Open();
			} catch (Exception error) {
				Console.Write(error.Message);
			}
        }

		public void sendMessage(){
		    	
			// Test, send a message
            var msg = new Message
            {
                Type = MessageType.Chat,
                To = "bauerj2@007jabber.com",
				Body = "From Xamarin"
            };

			try
            {
				xmppClient.Send(msg);
            }
            catch (Exception error)
            {
                Console.Write(error.Message);
            }
		}

		public void disconnect(){
			xmppClient.Close();
		}
	}
}
