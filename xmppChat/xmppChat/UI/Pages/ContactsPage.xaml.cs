﻿using System;
using System.Collections.Generic;
using xmppChat.ViewModels;

using Xamarin.Forms;

namespace xmppChat
{
    public partial class ContactsPage : ContentPage
    {
        public ContactsPage()
        {
            InitializeComponent();
			var contactsVM = new ContactsViewModel();
			BindingContext = contactsVM;
        }

	}
}
