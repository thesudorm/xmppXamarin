﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using xmppChat.ViewModels;

namespace xmppChat
{
    public partial class ConversationPage : ContentPage
    {
		void Handle_Completed(object sender, System.EventArgs e)
		{
			var messageContent = ((Entry)sender).Text;
            (BindingContext as ConversationViewModel).SendMessage(messageContent);
            ((Entry)sender).Text = "";
		}

        public ConversationPage(string toDisplay, string jid)
        {
            InitializeComponent();
			var conversationVM = new ConversationViewModel(toDisplay, jid);
			BindingContext = conversationVM;

        }

	}
}
