﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using xmppChat.ViewModels;

namespace xmppChat
{
    public partial class ConversationsPage : ContentPage
    {
        public ConversationsPage()
        {
            InitializeComponent();

			var conversationsVm = new ConversationsViewModel();
			conversationsVm.Navigation = Navigation;
			BindingContext = conversationsVm;
        }

		private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			//Do nothing for now
		}
	}
}